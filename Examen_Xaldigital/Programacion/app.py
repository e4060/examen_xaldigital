# Librerias
import requests as rq
import json as js
import datetime

print("\nExamen XALDIGITAL (Programación) \nSe deberá desarrollar un proceso de análisis de datos\n")

# -- 1. Conexión al enlace 
url = 'https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=perl&site=stackoverflow'
response = rq.get(url)

if response.status_code == 200:
    json_1 = response.json()
    print("Conexión al enlace: ")
    print(js.dumps({"Codigo" : response.status_code,
                    "Message": "[INFO] OK 200 "}))
elif response.status_code == 500:
    print(js.dumps({"Codigo" : response.status_code,
                    "Message": "[ERROR] Internal Server Error"}))


#-- 2. Obtener el número de respuestas contestadas y no contestadas
contestadas=len(list(filter(lambda x:x['is_answered']== True, json_1['items'])))
no_contestadas = len(list(filter(lambda x:x['is_answered'] == False, json_1['items'])))
print(f'\nNúmero de respuestas contestadas: \n {contestadas}')
print(f'\nNúmero de respuestas no contestadas: \n {no_contestadas}')

#--  3. Obtener la respuesta con menor número de vistas
menor_numero = min(json_1['items'], key= lambda s: s["view_count"])
print(f'\nRespuesta con menor número de vistas: \n {menor_numero}')

#-- 4. Obtener la respuesta más vieja y más actual
respuesta_vieja = min(json_1['items'], key= lambda s: datetime.datetime.fromtimestamp(s["creation_date"]).strftime('%Y-%m-%d %H:%M:%S'))
print("\nRespuesta Vieja: ")
print(respuesta_vieja)

respuesta_actual = max(json_1['items'], key= lambda s: datetime.datetime.fromtimestamp(s["creation_date"]).strftime('%Y-%m-%d %H:%M:%S'))
print("\n Respuesta Actual :")
print(respuesta_actual)

# -- 5. Obtener la respuesta del owner que tenga una mayor reputación
reputacion_alta = max(json_1['items'], key= lambda s: s['owner']["reputation"])
print("\nOwner con reputacion alta : ")
print(reputacion_alta)




